/*****************************************************
 *
 * MADX file for the post-LS2 LT line to generate the Survey Data
 *
 * Execute with:  >madx < lt.madx
 * This file is for H- at Ekin=160MeV
 *
 * - I. Efthymiopoulos - April 2019
 *****************************************************/

/*****************************************************************************
 * TITLE
 *****************************************************************************/
 title, 'LT 2019 optics. H- E_kin=160 MeV';

 option, -echo;
 !option, RBARC=FALSE;

 ldbfiles = 0; ! 1= use the new files in layoutDB directory, 0 = use the installed files
 dosurvey = 1; ! 1= execute the survey part

 value, ldbfiles;
 value, dosurvey;

/*****************************************************************************
 * LT
 * NB! The order of the .ele .str and .seq files matter.
 *****************************************************************************/
 if (ldbfiles == 1) {
  print text='>>> Running with LDB option.';
  call, file = '../layoutDB/lt_ldb.ele';
  call, file = '../layoutDB/lt_ldb.seq';
  call, file = '../layoutDB/lt_ldb.str';
 } else {
  print text='>>> Running standard option.';
  call, file = '../elements/lt.ele';
  call, file = '../sequence/lt.seq';
  call, file = '../strength/lt_ele.str';
 }
 call, file = '../strength/lt.str';
 call, file = '../aperture/lt.dbx';

/*******************************************************************************
 * Beam
 * NB! beam->ex == (beam->exn)/(beam->gamma*beam->beta*4)
 *******************************************************************************/
 BEAM, PARTICLE=Hminus, MASS=.93929, CHARGE=-1, PC=0.57083;! ENERGY=1.09929;
!show, beam;

/*******************************************************************************
 * survey
 *******************************************************************************/
 if (dosurvey == 1) {
     surlt : macro={
          use, sequence=LT;
          XS0 = -1915.79682; YS0 = 2433.66000; ZS0 = 2009.62939; THETAS0 = -6.633324745; !
          PHIS0   = 0.0;
          PSIS0   = 0.0;
          title, "LT BEAM LINE - LS2 CONFIGURATION";
          set,  format="-18s";
          set,  format="15.9f";
          ! set, format="30.20f";
          select, flag=survey,clear;
          select, flag=survey, column=NAME,S,L,ANGLE,X,Y,Z,THETA,PHI,PSI,GLOBALTILT,SLOT_ID, ASSEMBLY_ID;
          if (ldbfiles == 1){
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../test/lt_ldb_input_for_GEODE.sur";
            save,sequence=LT,file="../test/lt_ldb_sav.seq",bare,noexpr;
            assign, echo="../test/lt_ldb_dumpsequ.txt";
            dumpsequ, sequence=LT, level=1;
            assign, echo="terminal";
          } else {
            survey, x0=XS0, y0=YS0, z0=ZS0, theta0=THETAS0, phi0=PHIS0, psi0=PSIS0, file="../survey/lt_input_for_GEODE.sur";
            save,sequence=LT,file="../test/lt_sav.seq",bare,noexpr;
            assign, echo="../test/lt_dumpsequ.txt";
            dumpsequ, sequence=LT, level=1;
            assign, echo="terminal";
          }
     }
     exec, surlt;
 }

! stop;
