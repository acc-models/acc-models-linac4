import sys
sys.path.append("/Users/skowron/cern/rf-track-2.0/converters/madx2octave")
from cpymad.madx import Madx
import madx2octave

def main():
    madx = Madx()

    madx.input("conf.inuse.ring = 3;")
    madx.input("conf.dirlevel = 3;")
    madx.call("nominal_plus650kV.str")
    madx.call("../../../elements/all.seqx")
    madx.twiss(beta0="l4t.initbeta0")
    madx2octave.main(madx)

if __name__ == "__main__":
    main()
